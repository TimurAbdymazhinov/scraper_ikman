import json
import requests

from lxml import html
from loguru import logger


URL = 'https://ikman.lk/en/ads/sri-lanka/cars'


def get_html_data(url: str, params=None):
    """
    Function to get response from url

    :param url: source url
    :param params: option params
    :return: response
    """
    return requests.get(url, params=params, verify=False)


def get_content(html_data: str) -> list:
    """
    Function to get content from scraped page

    :param html_data:
    :return: list of scraped cars list
    """
    car_objects_list = []
    doc = html.fromstring(html_data)
    for element in doc.find_class('content--3JNQz'):
        try:
            title_e = element[0]
            description_e = element[1]
            n = 0 if len(description_e) == 3 else 1
            title = title_e.text
            mileage = description_e[0].text
            area, category = description_e[n+1].text.split(',')
            price = description_e[n+2].text_content()
            car_objects_list.append({
                'title': title, 'mileage': mileage, 'area': area.strip(),
                'category': category.strip(), 'price': price
            })
        except Exception as e:
            logger.error('Incorrect dom element')
            logger.exception(e)

    return car_objects_list


def write_to_json_file(file_name: str, objects_list: list) -> None:
    """
    Function to write cars list to file

    :param file_name:
    :param objects_list:
    :return: None
    """
    with open(file_name, "w") as outfile:
        json.dump(objects_list, outfile, indent=4)


def main():
    response = get_html_data(URL)
    if response.ok:
        car_objects_list = get_content(response.text)
        if car_objects_list:
            write_to_json_file('scrapped_cars.json', car_objects_list)
            logger.success('Successfully scraped and saved to file')
        else:
            logger.warning('Empty car_objects_list')

    else:
        logger.error('Error, response status is not success')


if __name__ == '__main__':
    main()
